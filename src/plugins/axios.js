import Vue from 'vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'
console.log()

import store from '../store'
console.log(store)
var axios=Axios.create({
  baseURL: 'https://cors-anywhere.herokuapp.com/https://quiet-thicket-97589.herokuapp.com',
  withCredentials: false,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'x-access-token':store.getters.getToken
  }
})
Vue.use(VueAxios, axios)

export default axios;
