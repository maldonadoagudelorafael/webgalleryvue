import Vue from 'vue'
import Vuex from 'vuex'
import axios from "./plugins/axios";
import router from "./router"

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cargando: false,
    logeado: false,
    articles: [],
    drawer: false,
    items: [
      {
        text: 'Home',
        to: '/',
        visible: true
      },
      {
        text: 'Login',
        href: '/login',
        to: '/login',
        visible: true
      },
      {
        text: 'perfil',
        href: '/perfil',
        to: '/perfil',
        visible: false
      }
      ,
      {
        text: 'mis obras',
        href: '/misobras',
        to: '/misobras',
        visible: false
      },
      {
        text: 'mis compras',
        href: '/miscompras',
        to: '/miscompras',
        visible: false
      },
      {
        text: 'Publicar',
        href: '/publicar',
        to: '/publicar',
        visible: false
      }
    ],
    cartProducts: [],
    currentProduct: {},
    showModal: false,
    showPopupCart: false,
    token: '',
    user:{
      username: "",
      name: "",
      email: "",
      password: "",
      country: "",
      city: "",
    },
    detalles:{}
  },
  getters: {
    logeado: (state) => {
      return state.logeado
    },
    links: (state, getters) => {
      return state.items
    },
    getToken: (state) => {
      return state.token
    },
    getProductsInCart: (state) => {
      return state.cartProducts

    }
  },
  mutations: {
    setDrawer: (state, payload) => (state.drawer = payload),
    toggleDrawer: state => (state.drawer = !state.drawer),
    ADD_PRODUCT: (state, product) => {
      console.log(product);
      state.cartProducts.push(product);
      console.log(state)
    },
    REMOVE_PRODUCT: (state, index) => {
      state.cartProducts.splice(index, 1);
    },
    CURRENT_PRODUCT: (state, product) => {
      state.currentProduct = product;
    },
    SHOW_MODAL: (state) => {
      state.showModal = !state.showModal;
    },
    SHOW_POPUP_CART: (state) => {
      state.showPopupCart = !state.showPopupCart;
    },
    loadProducts: (state) => {
      state.cargando = true
      axios
        .get('/products')
        .then(response => (

          state.articles = response.data))

      console.log(state)

      state.cargando = false
    },
    showCarga: (state) => {
      state.cargando = !state.cargando
    },

    setLogeo: (state, data) => {
      state.logeado = true
      state.token = data.accessToken
      state.user.username=data.username
      console.log(state.token)
      state.items[2].visible = true
      state.items[1].visible = false
      state.items[3].visible = true
      state.items[4].visible = true
      state.items[5].visible = true
      router.push({
        name: "perfil"
      })
    },
    setDetalles(state,detalles){
        state.detalles=detalles
    }

  },
  actions: {
    addProduct: (context, product) => {
      console.log('agregando');
      context.commit('ADD_PRODUCT', product);
    },
    removeProduct: (context, index) => {
      context.commit('REMOVE_PRODUCT', index);
    },
    currentProduct: (context, product) => {
      context.commit('CURRENT_PRODUCT', product);
    },
    showOrHiddenModal: (context) => {
      context.commit('SHOW_MODAL');
    },
    showOrHiddenPopupCart: (context) => {
      context.commit('SHOW_POPUP_CART');
    }
  }
})
