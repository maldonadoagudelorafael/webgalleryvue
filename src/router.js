import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import AcercaDe from "./views/AcercaDe"
import Login from "./views/Login";
import Carrito from "./views/Carrito";
import Registrar from "./views/Regitrar"
import Perfil from "./views/Perfil";
import MisObras from "./views/MisObras";
import MisCompras from "./views/MisCompras";
import Publicar from "./views/Publicar";
import Detalles from "./views/Detalles";
Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: AcercaDe
    },
    {
      path: '/login',
      name: 'login',
      component: Login

    },
    {
      path: '/carrito',
      name: 'carrito',
      component:Carrito

    }
    ,
    {
      path: '/registrar',
      name: 'registrar',
      component:Registrar

    }
    ,
    {
      path: '/perfil',
      name: 'perfil',
      component:Perfil

    } ,
    {
      path: '/misobras',
      name: 'misobras',
      component:MisObras

    },
    {
      path: '/miscompras',
      name: 'miscompras',
      component:MisCompras

    },
    {
      path: '/publicar',
      name: 'publicar',
      component:Publicar

    },
    {
      path: '/detalles/',
      name: 'detalles',
      component:Detalles

    }
  ]
})
